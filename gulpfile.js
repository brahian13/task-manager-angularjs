var gulp = require('gulp'),
 concat = require('gulp-concat'),
 minify = require('gulp-minify'),
 minifyCss = require('gulp-minify-css');

var path = {
	js: {
		src:['./www/app/**/*.js'],
		dest:'./www/dist/js'
	},
	css: {
		src:['./www/app/**/*.css'],
		dest:'./www/dist/css'
	}
}

gulp.task('default', function(){
	gulp.watch(path.js.src, ['concatjs']);
	gulp.watch(path.css.src, ['concatcss']);
});


gulp.task('concatjs', function(){
	gulp.src(path.js.src)
		.pipe(concat('taskMain.js'))
		.pipe(minify())
		.pipe(gulp.dest(path.js.dest));
});

gulp.task('concatcss', function(){
	gulp.src(path.css.src)
		.pipe(concat('taskMain.css'))
		.pipe(gulp.dest(path.css.dest))
		.pipe(minifyCss())
		.pipe(gulp.dest(path.css.dest));
});