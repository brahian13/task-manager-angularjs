(function(){

  angular.module('app')
  .controller('menuCtrl', Menu);

  Menu.$inject= ['$scope' , '$timeout', '$mdSidenav', '$location', '$rootScope'];

  function Menu($scope , $timeout, $mdSidenav, $location, $rootScope){

    $scope.cerrarSession = cerrarSession;
    $scope.Register=Register;
    $scope.TaskPeding=TaskPeding;
    $scope.TasksCompleted=TasksCompleted;
    $scope.ChangePassword=ChangePassword;
    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');
      
    function buildToggler(componentId) {
      return function() {
          $mdSidenav(componentId).toggle();
       }
    }
    function cerrarSession(){
      window.localStorage.removeItem("user");
        $location.path('Login');
    }
    function Register(){
        $location.path('Registrotarea');
    }
    function TaskPeding(){
        $location.path('Tareapendientes');
    }
    function TasksCompleted(){
        $location.path('TareaFinalizadas');
    }
    function ChangePassword(){
        $location.path('CambiarContrasenia');
    }
    
    
  };
})();