(function(){

	angular.module('app')
	.controller('tareasFinalizadasCTRL', TasksC);

	TasksC.$inject= ['jsonServices', '$location', '$rootScope'];

	function TasksC( jsonServices, $location, $rootScope){

		var vmTaskC= this;
		vmTaskC.tasks = [];
		vmTaskC.query = '';
		vmTaskC.username =$rootScope.user.username;

		getTask();

		function getTask(){
			var successCallback = function(response){
				vmTaskC.tasks = response;
			};
			jsonServices.getTasks(vmTaskC.username,'finalizado',successCallback);
		}
	};
})();