(function(){

	angular.module('app')
	.controller('loginCtrl', Login);

	Login.$inject= ['$location', '$rootScope', 'jsonServices'];

	function Login($location, $rootScope,  jsonServices){

		var vmLogin = this;
		vmLogin.user = {};
		vmLogin.login = login;

		function login(){
			if(vmLogin.user.username != undefined && vmLogin.user.password != undefined){
					var successCallback = function(response){
						if(response==undefined){
							alert("el usuario y contrasenia incorrectas");
						}else{
							$rootScope.user = response;
							vmLogin.user  = response;
							window.localStorage.setItem("user", JSON.stringify(vmLogin.user));
							$location.path('Tareapendientes');
						}
					};
					jsonServices.login(vmLogin.user.username ,vmLogin.user.password,successCallback);
				}else{
					alert("Ingresa el username && el password");
				}	
			};	
	};
})();