var applicationConfig = {
    "base_url":"http://localhost:3000/",
    "users": "users",
    "tasks": "tasks"
};

angular.module('app', [
    'ngRoute',
    'ngMaterial'
    ])
.value('taskData', {})
.constant('applicationConfig', applicationConfig)
.config(['$routeProvider', function($routeProvider) {
  	
    $routeProvider
    .when("/Registrotarea", {
        templateUrl : "app/registroTarea/registroTarea.html",
        controller: 'registroTareasCTRL',
        controllerAs: 'registroTarea'
    })
    .when("/Login", {
        templateUrl : "app/login/login.html",
        controller:'loginCtrl',
        controllerAs:'login' 
    })
    .when("/Detalletarea/:name", {
        templateUrl : "app/detalleTarea/detalleTarea.html",
        controller:'detalleCTRL',
        controllerAs:'detalleTarea'
    })
    .when("/Tareapendientes", {
        templateUrl : "app/tareaPendientes/tareaPendientes.html",
        controller:'tareasPendientesCTRL',
        controllerAs:'tareasPendientes'
    })
    .when("/TareaFinalizadas", {
        templateUrl : "app/tareaFinalizadas/tareaFinalizadas.html",
        controller:'tareasFinalizadasCTRL',
        controllerAs:'tareasFinalizadas'
    })
    .when("/CambiarContrasenia", {
        templateUrl : "app/cambioContrasenia/cambioContrasenia.html",
        controller:'cambiarCTRL',
        controllerAs:'cambioContrasenia'
    })
    .when("/Edittask", {
        templateUrl : "app/editTask/editTask.html",
        controller:'editTaskCtrl',
        controllerAs:'editTask'
    })
    .otherwise({redirectTo: '/Login'});
}]);

