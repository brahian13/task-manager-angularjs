(function(){

    angular.module('app')
        .run(['$rootScope', '$location',function($rootScope, $location){

        var user = window.localStorage.getItem("user");
        if(user !== null){
            user = JSON.parse(user);
            $rootScope.user = user;     
        } 

       $rootScope.$on('$routeChangeStart', function (event) {
            user = window.localStorage.getItem("user");
            if ($location.$$path !== '/Login' && user === null) {
                event.preventDefault();
                $location.path('Login');
            } else if(user !== null){
                if($location.$$path === '/Login'){
                    event.preventDefault();
                    $location.path('Tareapendientes');
                } else {
                    return;
                }
            }
        });
    }]);
})();
