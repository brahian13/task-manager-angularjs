(function(){

	angular.module('app')
	.controller('editTaskCtrl', Task);

	Task.$inject= ['jsonServices', 'taskData', '$location','$mdDialog'];

	function Task( jsonServices, taskData, $location, $mdDialog ){

		var vmTask= this;
		vmTask.taskDetails=taskData.task;
		vmTask.task={};
		vmTask.update=update;

		function update(ev){
			var successCallback = function(){
					vmTask.task = vmTask.taskDetails;
					showAlert(ev);
					$location.path('Detalletarea/'+ vmTask.task.name);
				};
				jsonServices.update(vmTask.taskDetails,successCallback);
		}	

		function showAlert(ev) {
		    $mdDialog.show(
		      $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#popupContainer')))
		        .clickOutsideToClose(true)
		        .title('Se actualizo la tarea')
		        .textContent('tarea actualizada')
		        .ariaLabel('Alert Dialog Demo')
		        .ok('Ok')
		        .targetEvent(ev)
		    );
		  };


	};
})();