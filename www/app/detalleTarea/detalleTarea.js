(function(){

	angular.module('app')
	.controller('detalleCTRL', Task);

	Task.$inject= ['jsonServices', 'taskData', '$location'];

	function Task( jsonServices, taskData ,$location){

		var vmTask= this;
		vmTask.taskDetails={};
		vmTask.getTarea=getTarea;
		vmTask.editTask=editTask;

		getTarea();

		function getTarea(){
			var successCallback = function(response){
					vmTask.taskDetails=response;
					console.log(vmTask.taskDetails.description);
				};
				jsonServices.task(taskData.task.id,successCallback);
		}	

		function editTask(task){
			taskData.taskEdit = task;
			$location.path('Edittask');
		}
	};
})();