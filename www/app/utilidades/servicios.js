(function(){

	angular.module('app')
	.service('jsonServices', Services);

	Services.$inject= ['$http', 'applicationConfig'];

	function Services($http, applicationConfig){
		
		var service = {
			login:login,
			task:task,
			getTasks:getTasks,
			buscarTasks:buscarTasks,
			add:add,
			update:update,
			updateUser:updateUser
		};
		return service;

		function login(username, password, successFunction){
			$http({
						method: 'GET',
						url: applicationConfig.base_url+ "users?username=" +username+"&password="+password
					}).then(function successcallback(response) {
						
						if (successFunction != undefined) {
								successFunction(response.data[0]);
							};
					}, function errorCallback(response) {
						console.log(response.data);
				});
		}

		function task(id, successFunction){
			$http({
				method: 'GET',
				url: applicationConfig.base_url+ "tasks/"+ id 
			}).then(function successcallback(response) {
				
				if (successFunction != undefined) {
						successFunction(response.data);
					};
			}, function errorCallback(response) {
				console.log(response.data);
			});
		}

		function getTasks(username, estado, successFunction){
			$http({
				method: 'GET',
				url: applicationConfig.base_url+ "tasks?username="+ username + "&estado="+ estado
			}).then(function successcallback(response) {
				
				if (successFunction != undefined) {
						successFunction(response.data);
					};
			}, function errorCallback(response) {
				console.log(response.data);
			});
		}

		function buscarTasks(username, estado, start, end, successFunction){
			$http({
				method: 'GET',
				url: applicationConfig.base_url+ "tasks?username="+ username + "&estado="+ estado+ '&?_start='+start+'&_end='+end
			}).then(function successcallback(response) {
				
				if (successFunction != undefined) {
						successFunction(response.data);
					};
			}, function errorCallback(response) {
				console.log(response.data);
			});
		}


		function add(ruta, datos, successFunction){
			$http({
				method: 'POST',
				url: applicationConfig.base_url + ruta,
				data: angular.toJson(datos, true)
			}).then(function successcallback(response) {
				if (angular.fromJson(response.data).status ==="OK") {
					if (successFunction != undefined) {
						successFunction();
					};
				}
			});
		}

		function update(task,successFunction){
			
	 		$http({
				method: 'PUT',
				url: applicationConfig.base_url +'tasks/'+task.id,
				data: angular.toJson(task, true)
			}).then(function successcallback(response) {
					if (successFunction != undefined) {
						successFunction();
					};
			}, function errorCallback(response) {
				if (angular.fromJson(response.data).status==="ERROR") {
					alert(angular.fromJson(response.data).messages[0]);
				}
			});
		}

		function updateUser(user,successFunction){
			
	 		$http({
				method: 'PUT',
				url: applicationConfig.base_url +'users/'+user.id,
				data: angular.toJson(user, true)
			}).then(function successcallback(response) {
					if (successFunction != undefined) {
						successFunction();
					};
			}, function errorCallback(response) {
				if (angular.fromJson(response.data).status==="ERROR") {
					alert(angular.fromJson(response.data).messages[0]);
				}
			});
		}

	};
})();