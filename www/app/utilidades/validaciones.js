(function(){

	angular.module('app')
	.directive('specialChar', [SpecialChar]);

 	function SpecialChar() {

	    var link = function(scope, element, attrs, ctrl) {

	      	var validate = function(viewValue) {

		        if(!viewValue){
		          	ctrl.$setValidity('specialChar', true);
		          	return viewValue;
		        }
		        var validateExpresion = "" + viewValue;

		        ctrl.$setValidity('specialChar', validateExpresion.replace(/[^~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g, "").length >= 1);
		        return viewValue;
		    };

	    	ctrl.$parsers.unshift(validate);
	    	ctrl.$formatters.push(validate);
	    };

	    return {
	      require: 'ngModel',
	      link: link
	    };
  }
})();