(function(){

	angular.module('app')
	.controller('tareasPendientesCTRL', TasksP);

	TasksP.$inject= ['jsonServices', 'taskData','$location', '$rootScope', '$mdDialog'];

	function TasksP( jsonServices, taskData,$location, $rootScope, $mdDialog){

		var vmTaskP= this;
		vmTaskP.tasks = [];
		vmTaskP.task = {};
		vmTaskP.taskUpdate = {};
		vmTaskP.taskDetail = taskDetail;
		vmTaskP.taskCompleted = taskCompleted;
		vmTaskP.query = '';
		vmTaskP.showConfirm = showConfirm;
		vmTaskP.paginator = 0;
		vmTaskP.loadTask= loadTask;
		vmTaskP.username=$rootScope.user.username;

		getTask();

		function getTask(ev){

			var successCallback = function(response){
			//	var tot=Object.keys(response).length;
				if (Object.keys(response).length>0) {
					if((end-1)<Object.keys(response).length){
							concatenarDato(response);
					}else{
						if(end-Object.keys(response).length<5){
							concatenarDato(response);	
						}else{
							showAlert(ev);
						}						
					}
				}
			};

			var end = vmTaskP.paginator + 10;
			var start=0 ;
			
			jsonServices.buscarTasks(vmTaskP.username,'abierta', start, end, successCallback);
		}

		function taskDetail(task){
			taskData.task = task;
			$location.path('Detalletarea/'+ task.name);
		}	

		function taskCompleted(task,ev){

			var successCallback = function(){
					getTask();
				};
			vmTaskP.task=task;
			vmTaskP.task.estado="finalizado";
			jsonServices.update(vmTaskP.task,successCallback);
			
		}

		function loadTask(ev){
			vmTaskP.paginator += 5;
			getTask(ev);
		};

		function concatenarDato(data){
				vmTaskP.tasks = data;
		}

		function showAlert(ev) {
		    $mdDialog.show(
		      $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#popupContainer')))
		        .clickOutsideToClose(true)
		        .title('No hay Mas Tareas')
		        .textContent('Todas las tareas han sido Cargadas')
		        .ariaLabel('Alert Dialog Demo')
		        .ok('Ok')
		        .targetEvent(ev)
		    );
		  };


		 function showConfirm(ev,task) {
			    // Appending dialog to document.body to cover sidenav in docs app
			    var confirm = $mdDialog.confirm()
			          .title('Terminaste la tarea')
			          .textContent('Si finalizo la tarea click en finalizado, No cancelar')
			          .ariaLabel('Lucky day')
			          .targetEvent(ev)
			          .ok('Finalizado')
			          .cancel('Cancelar');
			    $mdDialog.show(confirm).then(function() {
			      	taskCompleted(task, ev);
			    }, function() {
			      
			    });
			 };
	};
})();