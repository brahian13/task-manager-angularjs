(function(){

	angular.module('app')
	.controller('registroTareasCTRL', RegisterTask);

	RegisterTask.$inject= ['jsonServices',  '$rootScope', '$mdDialog'];

	function RegisterTask( jsonServices,  $rootScope, $mdDialog){

		var vmRegisterTask= this;
		vmRegisterTask.task = {};
		vmRegisterTask.registerTask=registerTask;
		var date;


		function registerTask(ev){
				if(vmRegisterTask.task.name!=undefined && vmRegisterTask.task.fecha!=undefined && vmRegisterTask.task.description!=undefined){
					date = vmRegisterTask.task.fecha;

					vmRegisterTask.dd = date.getDate();
					vmRegisterTask.mm = date.getMonth()+1; 
					vmRegisterTask.yyyy = date.getFullYear();

					vmRegisterTask.date = vmRegisterTask.dd+'/'+vmRegisterTask.mm+'/'+vmRegisterTask.yyyy;

					var successCallback = function(){
						};
					vmRegisterTask.task.username= $rootScope.user.username;
					vmRegisterTask.task.estado= "abierta";
					vmRegisterTask.task.fecha=vmRegisterTask.date;
					jsonServices.add('tasks',vmRegisterTask.task,successCallback);
					vmRegisterTask.task={};
					showAlert(ev);
				}else {
					alert("Debe diligenciar todos los campos");
				}
			}

			function showAlert(ev) {
		    $mdDialog.show(
		      $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#popupContainer')))
		        .clickOutsideToClose(true)
		        .title('Se registro la tarea')
		        .textContent('La tarea ha sido registrado exitosamente')
		        .ariaLabel('Alert Dialog Demo')
		        .ok('Ok')
		        .targetEvent(ev)
		    );
		  };
	};
})();