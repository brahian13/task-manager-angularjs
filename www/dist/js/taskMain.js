var applicationConfig = {
    "base_url":"http://localhost:3000/",
    "users": "users",
    "tasks": "tasks"
};

angular.module('app', [
    'ngRoute',
    'ngMaterial'
    ])
.value('taskData', {})
.constant('applicationConfig', applicationConfig)
.config(['$routeProvider', function($routeProvider) {
  	
    $routeProvider
    .when("/Registrotarea", {
        templateUrl : "app/registroTarea/registroTarea.html",
        controller: 'registroTareasCTRL',
        controllerAs: 'registroTarea'
    })
    .when("/Login", {
        templateUrl : "app/login/login.html",
        controller:'loginCtrl',
        controllerAs:'login' 
    })
    .when("/Detalletarea/:name", {
        templateUrl : "app/detalleTarea/detalleTarea.html",
        controller:'detalleCTRL',
        controllerAs:'detalleTarea'
    })
    .when("/Tareapendientes", {
        templateUrl : "app/tareaPendientes/tareaPendientes.html",
        controller:'tareasPendientesCTRL',
        controllerAs:'tareasPendientes'
    })
    .when("/TareaFinalizadas", {
        templateUrl : "app/tareaFinalizadas/tareaFinalizadas.html",
        controller:'tareasFinalizadasCTRL',
        controllerAs:'tareasFinalizadas'
    })
    .when("/CambiarContrasenia", {
        templateUrl : "app/cambioContrasenia/cambioContrasenia.html",
        controller:'cambiarCTRL',
        controllerAs:'cambioContrasenia'
    })
    .when("/Edittask", {
        templateUrl : "app/editTask/editTask.html",
        controller:'editTaskCtrl',
        controllerAs:'editTask'
    })
    .otherwise({redirectTo: '/Login'});
}]);


(function(){

    angular.module('app')
        .run(['$rootScope', '$location',function($rootScope, $location){

        var user = window.localStorage.getItem("user");
        if(user !== null){
            user = JSON.parse(user);
            $rootScope.user = user;     
        } 

       $rootScope.$on('$routeChangeStart', function (event) {
            user = window.localStorage.getItem("user");
            if ($location.$$path !== '/Login' && user === null) {
                event.preventDefault();
                $location.path('Login');
            } else if(user !== null){
                if($location.$$path === '/Login'){
                    event.preventDefault();
                    $location.path('Tareapendientes');
                } else {
                    return;
                }
            }
        });
    }]);
})();

(function(){

	angular.module('app')
	.controller('cambiarCTRL', Cambiar);

	Cambiar.$inject= ['jsonServices', '$rootScope'];

	function Cambiar( jsonServices, $rootScope){

		var vmUser= this;
		vmUser.user = {};
		vmUser.cambiar=cambiar;
		vmUser.userUpdate = {};

		function cambiar(){
			
			var successCallback = function(){
					vmUser.userUpdate;
				};
				vmUser.userUpdate=$rootScope.user;
				vmUser.userUpdate.password=vmUser.user.password;
				jsonServices.updateUser(vmUser.userUpdate,successCallback);
		}	
	};
})();
(function(){

	angular.module('app')
	.controller('detalleCTRL', Task);

	Task.$inject= ['jsonServices', 'taskData', '$location'];

	function Task( jsonServices, taskData ,$location){

		var vmTask= this;
		vmTask.taskDetails={};
		vmTask.getTarea=getTarea;
		vmTask.editTask=editTask;

		getTarea();

		function getTarea(){
			var successCallback = function(response){
					vmTask.taskDetails=response;
					console.log(vmTask.taskDetails.description);
				};
				jsonServices.task(taskData.task.id,successCallback);
		}	

		function editTask(task){
			taskData.taskEdit = task;
			$location.path('Edittask');
		}
	};
})();
(function(){

	angular.module('app')
	.controller('editTaskCtrl', Task);

	Task.$inject= ['jsonServices', 'taskData', '$location','$mdDialog'];

	function Task( jsonServices, taskData, $location, $mdDialog ){

		var vmTask= this;
		vmTask.taskDetails=taskData.task;
		vmTask.task={};
		vmTask.update=update;

		function update(ev){
			var successCallback = function(){
					vmTask.task = vmTask.taskDetails;
					showAlert(ev);
					$location.path('Detalletarea/'+ vmTask.task.name);
				};
				jsonServices.update(vmTask.taskDetails,successCallback);
		}	

		function showAlert(ev) {
		    $mdDialog.show(
		      $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#popupContainer')))
		        .clickOutsideToClose(true)
		        .title('Se actualizo la tarea')
		        .textContent('tarea actualizada')
		        .ariaLabel('Alert Dialog Demo')
		        .ok('Ok')
		        .targetEvent(ev)
		    );
		  };


	};
})();
(function(){

	angular.module('app')
	.controller('loginCtrl', Login);

	Login.$inject= ['$location', '$rootScope', 'jsonServices'];

	function Login($location, $rootScope,  jsonServices){

		var vmLogin = this;
		vmLogin.user = {};
		vmLogin.login = login;

		function login(){
			if(vmLogin.user.username != undefined && vmLogin.user.password != undefined){
					var successCallback = function(response){
						if(response==undefined){
							alert("el usuario y contrasenia incorrectas");
						}else{
							$rootScope.user = response;
							vmLogin.user  = response;
							window.localStorage.setItem("user", JSON.stringify(vmLogin.user));
							$location.path('Tareapendientes');
						}
					};
					jsonServices.login(vmLogin.user.username ,vmLogin.user.password,successCallback);
				}else{
					alert("Ingresa el username && el password");
				}	
			};	
	};
})();
(function(){

  angular.module('app')
  .controller('menuCtrl', Menu);

  Menu.$inject= ['$scope' , '$timeout', '$mdSidenav', '$location', '$rootScope'];

  function Menu($scope , $timeout, $mdSidenav, $location, $rootScope){

    $scope.cerrarSession = cerrarSession;
    $scope.Register=Register;
    $scope.TaskPeding=TaskPeding;
    $scope.TasksCompleted=TasksCompleted;
    $scope.ChangePassword=ChangePassword;
    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');
      
    function buildToggler(componentId) {
      return function() {
          $mdSidenav(componentId).toggle();
       }
    }
    function cerrarSession(){
      window.localStorage.removeItem("user");
        $location.path('Login');
    }
    function Register(){
        $location.path('Registrotarea');
    }
    function TaskPeding(){
        $location.path('Tareapendientes');
    }
    function TasksCompleted(){
        $location.path('TareaFinalizadas');
    }
    function ChangePassword(){
        $location.path('CambiarContrasenia');
    }
    
    
  };
})();
(function(){

	angular.module('app')
	.controller('registroTareasCTRL', RegisterTask);

	RegisterTask.$inject= ['jsonServices',  '$rootScope', '$mdDialog'];

	function RegisterTask( jsonServices,  $rootScope, $mdDialog){

		var vmRegisterTask= this;
		vmRegisterTask.task = {};
		vmRegisterTask.registerTask=registerTask;
		var date;


		function registerTask(ev){
				if(vmRegisterTask.task.name!=undefined && vmRegisterTask.task.fecha!=undefined && vmRegisterTask.task.description!=undefined){
					date = vmRegisterTask.task.fecha;

					vmRegisterTask.dd = date.getDate();
					vmRegisterTask.mm = date.getMonth()+1; 
					vmRegisterTask.yyyy = date.getFullYear();

					vmRegisterTask.date = vmRegisterTask.dd+'/'+vmRegisterTask.mm+'/'+vmRegisterTask.yyyy;

					var successCallback = function(){
						};
					vmRegisterTask.task.username= $rootScope.user.username;
					vmRegisterTask.task.estado= "abierta";
					vmRegisterTask.task.fecha=vmRegisterTask.date;
					jsonServices.add('tasks',vmRegisterTask.task,successCallback);
					vmRegisterTask.task={};
					showAlert(ev);
				}else {
					alert("Debe diligenciar todos los campos");
				}
			}

			function showAlert(ev) {
		    $mdDialog.show(
		      $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#popupContainer')))
		        .clickOutsideToClose(true)
		        .title('Se registro la tarea')
		        .textContent('La tarea ha sido registrado exitosamente')
		        .ariaLabel('Alert Dialog Demo')
		        .ok('Ok')
		        .targetEvent(ev)
		    );
		  };
	};
})();
(function(){

	angular.module('app')
	.controller('tareasFinalizadasCTRL', TasksC);

	TasksC.$inject= ['jsonServices', '$location', '$rootScope'];

	function TasksC( jsonServices, $location, $rootScope){

		var vmTaskC= this;
		vmTaskC.tasks = [];
		vmTaskC.query = '';
		vmTaskC.username =$rootScope.user.username;

		getTask();

		function getTask(){
			var successCallback = function(response){
				vmTaskC.tasks = response;
			};
			jsonServices.getTasks(vmTaskC.username,'finalizado',successCallback);
		}
	};
})();
(function(){

	angular.module('app')
	.controller('tareasPendientesCTRL', TasksP);

	TasksP.$inject= ['jsonServices', 'taskData','$location', '$rootScope', '$mdDialog'];

	function TasksP( jsonServices, taskData,$location, $rootScope, $mdDialog){

		var vmTaskP= this;
		vmTaskP.tasks = [];
		vmTaskP.task = {};
		vmTaskP.taskUpdate = {};
		vmTaskP.taskDetail = taskDetail;
		vmTaskP.taskCompleted = taskCompleted;
		vmTaskP.query = '';
		vmTaskP.showConfirm = showConfirm;
		vmTaskP.paginator = 0;
		vmTaskP.loadTask= loadTask;
		vmTaskP.username=$rootScope.user.username;

		getTask();

		function getTask(ev){

			var successCallback = function(response){
			//	var tot=Object.keys(response).length;
				if (Object.keys(response).length>0) {
					if((end-1)<Object.keys(response).length){
							concatenarDato(response);
					}else{
						if(end-Object.keys(response).length<5){
							concatenarDato(response);	
						}else{
							showAlert(ev);
						}						
					}
				}
			};

			var end = vmTaskP.paginator + 10;
			var start=0 ;
			
			jsonServices.buscarTasks(vmTaskP.username,'abierta', start, end, successCallback);
		}

		function taskDetail(task){
			taskData.task = task;
			$location.path('Detalletarea/'+ task.name);
		}	

		function taskCompleted(task,ev){

			var successCallback = function(){
					getTask();
				};
			vmTaskP.task=task;
			vmTaskP.task.estado="finalizado";
			jsonServices.update(vmTaskP.task,successCallback);
			
		}

		function loadTask(ev){
			vmTaskP.paginator += 5;
			getTask(ev);
		};

		function concatenarDato(data){
				vmTaskP.tasks = data;
		}

		function showAlert(ev) {
		    $mdDialog.show(
		      $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#popupContainer')))
		        .clickOutsideToClose(true)
		        .title('No hay Mas Tareas')
		        .textContent('Todas las tareas han sido Cargadas')
		        .ariaLabel('Alert Dialog Demo')
		        .ok('Ok')
		        .targetEvent(ev)
		    );
		  };


		 function showConfirm(ev,task) {
			    // Appending dialog to document.body to cover sidenav in docs app
			    var confirm = $mdDialog.confirm()
			          .title('Terminaste la tarea')
			          .textContent('Si finalizo la tarea click en finalizado, No cancelar')
			          .ariaLabel('Lucky day')
			          .targetEvent(ev)
			          .ok('Finalizado')
			          .cancel('Cancelar');
			    $mdDialog.show(confirm).then(function() {
			      	taskCompleted(task, ev);
			    }, function() {
			      
			    });
			 };
	};
})();
(function(){

	angular.module('app')
	.service('jsonServices', Services);

	Services.$inject= ['$http', 'applicationConfig'];

	function Services($http, applicationConfig){
		
		var service = {
			login:login,
			task:task,
			getTasks:getTasks,
			buscarTasks:buscarTasks,
			add:add,
			update:update,
			updateUser:updateUser
		};
		return service;

		function login(username, password, successFunction){
			$http({
						method: 'GET',
						url: applicationConfig.base_url+ "users?username=" +username+"&password="+password
					}).then(function successcallback(response) {
						
						if (successFunction != undefined) {
								successFunction(response.data[0]);
							};
					}, function errorCallback(response) {
						console.log(response.data);
				});
		}

		function task(id, successFunction){
			$http({
				method: 'GET',
				url: applicationConfig.base_url+ "tasks/"+ id 
			}).then(function successcallback(response) {
				
				if (successFunction != undefined) {
						successFunction(response.data);
					};
			}, function errorCallback(response) {
				console.log(response.data);
			});
		}

		function getTasks(username, estado, successFunction){
			$http({
				method: 'GET',
				url: applicationConfig.base_url+ "tasks?username="+ username + "&estado="+ estado
			}).then(function successcallback(response) {
				
				if (successFunction != undefined) {
						successFunction(response.data);
					};
			}, function errorCallback(response) {
				console.log(response.data);
			});
		}

		function buscarTasks(username, estado, start, end, successFunction){
			$http({
				method: 'GET',
				url: applicationConfig.base_url+ "tasks?username="+ username + "&estado="+ estado+ '&?_start='+start+'&_end='+end
			}).then(function successcallback(response) {
				
				if (successFunction != undefined) {
						successFunction(response.data);
					};
			}, function errorCallback(response) {
				console.log(response.data);
			});
		}


		function add(ruta, datos, successFunction){
			$http({
				method: 'POST',
				url: applicationConfig.base_url + ruta,
				data: angular.toJson(datos, true)
			}).then(function successcallback(response) {
				if (angular.fromJson(response.data).status ==="OK") {
					if (successFunction != undefined) {
						successFunction();
					};
				}
			});
		}

		function update(task,successFunction){
			
	 		$http({
				method: 'PUT',
				url: applicationConfig.base_url +'tasks/'+task.id,
				data: angular.toJson(task, true)
			}).then(function successcallback(response) {
					if (successFunction != undefined) {
						successFunction();
					};
			}, function errorCallback(response) {
				if (angular.fromJson(response.data).status==="ERROR") {
					alert(angular.fromJson(response.data).messages[0]);
				}
			});
		}

		function updateUser(user,successFunction){
			
	 		$http({
				method: 'PUT',
				url: applicationConfig.base_url +'users/'+user.id,
				data: angular.toJson(user, true)
			}).then(function successcallback(response) {
					if (successFunction != undefined) {
						successFunction();
					};
			}, function errorCallback(response) {
				if (angular.fromJson(response.data).status==="ERROR") {
					alert(angular.fromJson(response.data).messages[0]);
				}
			});
		}

	};
})();
(function(){

	angular.module('app')
	.directive('specialChar', [SpecialChar]);

 	function SpecialChar() {

	    var link = function(scope, element, attrs, ctrl) {

	      	var validate = function(viewValue) {

		        if(!viewValue){
		          	ctrl.$setValidity('specialChar', true);
		          	return viewValue;
		        }
		        var validateExpresion = "" + viewValue;

		        ctrl.$setValidity('specialChar', validateExpresion.replace(/[^~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g, "").length >= 1);
		        return viewValue;
		    };

	    	ctrl.$parsers.unshift(validate);
	    	ctrl.$formatters.push(validate);
	    };

	    return {
	      require: 'ngModel',
	      link: link
	    };
  }
})();